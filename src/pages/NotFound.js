/*import Banner from "../components/Banner";

export default function Error() {

	const date = {
		title: "Error 404 - Page not found.",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}


}
*/












import {useNavigate} from 'react-router-dom';

import {Link, NavLink} from 'react-router-dom';

import { Button, Row, Col } from 'react-bootstrap';


export default function NotFound() {
return (
	<Row>
	    <Col className="p-5">
	        <h1>Error 404 - Page Not Found</h1>
	        <p>The page you are looking for cannot be found.</p>
	     <Button><Link class="text-decoration-none text-light" as={NavLink} to="/">Back to Home</Link></Button>
	    </Col>
	</Row>
)
};