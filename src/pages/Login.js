import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'

// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from "../UserContext";

import Swal from 'sweetalert2';

export default function Login(){

    const {user, setUser} = useContext(UserContext);

    // to store values of the input fields
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // hook returns a function that lets you navigate to components
    // const navigate = useNavigate();

    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                }) 
            } else {
            Swal.fire({
                title: "Login Successful!",
                icon: "success",
                text: "Welcome to Zuitt!" 
            })
        }
               
    });


        // Set the email of the authenticated user in the local storage
        // localStorage.setItem('propertyName', value)
        // localStorage.setItem('email', email);

        // sets the global user state to have properties obtain from local storage
        // setUser({email: localStorage.getItem("email")});

        setEmail('');
        setPassword('');
        
        // navigate('/');

        // alert("Successfully login!")
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
}












// Activity
/*import {useState, useEffect} from "react";
import { Form, Button } from 'react-bootstrap';

export default function Login() {

     const [email, setEmail] = useState("");
     const [password1, setPassword1] = useState("");
     // to determine wheter submit button is enabled or not
     const [isActive, setIsActive] = useState(false);

     useEffect(() => {
    if(email !== "" && password1 !== "") {
         setIsActive(true);
    } else {
         setIsActive(false);
    }
     }, [email, password1]);

     // function to stimulate user registration
     function registerUser(e) {
         e.preventDefault();

         // Clear input fields
         setEmail("");
         setPassword1("");

         alert("Thank you for logging in");
     };

  return (
        <Form onSubmit={(e) => registerUser(e)} >
        <h1>Login</h1>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>


            {isActive ?
                <Button variant="success" type="submit" id="submitBtn">
                    Login
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Login
                </Button>
            }

           
        </Form>
    )

};*/